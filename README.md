# Bloc 1 Developpement
Bien que le Bloc 1 du BTS SIO soit un tronc commun, notre établissement a fait le choix de découper les blocs par domaine :
- Systèmes (SYS)
- Réseaux (RES)
- Développement (DEV)
- Gestion de données (BDD)

Ainsi, vous trouverez ici mes cours pour la partie Développement.
